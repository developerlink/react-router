import React from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import Products from "./pages/Products";
import Welcome from "./pages/Welcome";
import MainHeader from "./components/MainHeader";
import ProductDetail from "./pages/ProductDetail";

function App() {

  return (
    <div>
      <MainHeader />
      <Routes>
        <Route path="/" element={<Navigate replace to="/welcome" />} />
        <Route
          path="/welcome"
          element={<Welcome text="hello world" />}
        >
          <Route path={"products"} element={<p>Welcome</p>} />
        </Route>
        <Route exact path="/products" element={<Products />} />
        <Route exact path="/products/:productId" element={<ProductDetail />} />
      </Routes>
    </div>
  );
}

export default App;
