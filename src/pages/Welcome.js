import React from "react";
import { Outlet } from "react-router-dom";

const Welcome = (props) => {
  return (
    <React.Fragment>
      <section className="main">
      <h1 className="main">The Welcome Page</h1>    
      <Outlet />
      </section>
    </React.Fragment>
  );
};

export default Welcome;
